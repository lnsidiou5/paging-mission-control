# Paging Mission Control Submission - Jason Li
This README provides information setup and run the application.
This application was tested and compiled using Java 8 and Maven 4.0.0.

## Inputs and Outputs
### Inputs
The input text file should be placed in the ```data/input``` folder.
See the Run section to learn how to run a custom test on that text file.

### Output
The program will output to the console.

## Build
The application has already been compiled into a jar file and can directly be run.
You can use maven to build the project, just be aware that the resulting jar file needs to be moved to the main folder 
(since it searches for the input file from there to ```/data/input/testFileNameHere.txt)```.

## Run
### Run the default test case.
To run the default test, run the following in the command line:
```java -jar .\paging-mission-control-1.jar```

### Run a custom test case.
To run a custom test, add the text file to the data/input/ folder.
Then run the following in the command line:
```java -jar .\paging-mission-control-1.jar [FileName].txt```
For example, the default test can also be run the following way:
```java -jar .\paging-mission-control-1.jar test1.txt```

