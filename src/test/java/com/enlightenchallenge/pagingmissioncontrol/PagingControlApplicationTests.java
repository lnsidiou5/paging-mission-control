package com.enlightenchallenge.pagingmissioncontrol;

import com.enlightenchallenge.pagingmissioncontrol.entity.ResponseData;
import com.enlightenchallenge.pagingmissioncontrol.entity.TelemetryData;
import com.enlightenchallenge.pagingmissioncontrol.service.StatusService;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.SQLOutput;
import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PagingControlApplicationTests {

	@Test
	public void testCase1() throws Exception {
		StatusService svc = new StatusService();
		String filename= ".\\data\\input\\test1.txt";
		File input = new File(filename);

		List<ResponseData> out = svc.getStatus(input);
		assertEquals(2, out.size());
		assertEquals("TSTAT", out.get(0).getComponent());
		assertEquals(1000, out.get(0).getSatelliteID());
		assertEquals("RED HIGH", out.get(0).getSeverity());
		assertEquals("2018-01-01T23:01:38.001Z",out.get(0).getTimestamp().toString());
		assertEquals("BATT", out.get(1).getComponent());
		assertEquals(1000, out.get(1).getSatelliteID());
		assertEquals("RED LOW", out.get(1).getSeverity());
		assertEquals("2018-01-01T23:01:09.521Z",out.get(1).getTimestamp().toString());

	}

}
