package com.enlightenchallenge.pagingmissioncontrol.service;


import com.enlightenchallenge.pagingmissioncontrol.entity.ResponseData;
import com.enlightenchallenge.pagingmissioncontrol.entity.TelemetryData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.*;


public class StatusService {
    private static final int INTERVAL = 5;
    private static final int ERROR_LIMIT = 3;
    private static final String THERMOSTAT_VAL = "TSTAT";
    private static final String BATTERY_VAL = "BATT";
    private static final String RED_H = "RED HIGH";
    private static final String RED_L = "RED LOW";
    private List<TelemetryData> currentStats = new ArrayList<>();

    public List<ResponseData> getStatus(File file) {
        try {
            // Read the file into an input stream
            InputStream inputStream = new FileInputStream(file);
            // Use a buffer reader to read each line and convert it into telemetry data objects.
            new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .forEach(this::getStatFromLine);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // return the result after finding all issues.
        return checkForIssues();
    }
    // Generates the telemetry data from the string.
    private void getStatFromLine(String line) {
        TelemetryData parsedStatus = null;
        try {
            //Split based on the vertical lines.
            parsedStatus = new TelemetryData(line.split("\\|"));
            currentStats.add(parsedStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<ResponseData> checkForIssues() {
        //Store values that have red severity by ID
        Map<Integer, Deque<TelemetryData>> batteryAlerts = new HashMap<>();
        Map<Integer, Deque<TelemetryData>> tstatAlerts = new HashMap<>();
        List<ResponseData> results = new ArrayList<>();
        // Sort ascending based on time, although it should already be. But just in case.
        currentStats.sort((td0, td1) -> td0.getTimestamp().compareTo(td1.getTimestamp()));
        // iterate through all data.
        for (TelemetryData status : currentStats) {
            // Report for violation doesn't exist unless we find one.
            ResponseData violation = null;
            if (status.getRawValue() > status.getRedHighLimit() && status.getComponent().equals(THERMOSTAT_VAL)) {
                // Thermostat indicates it is too high.
                violation = checkIssue(tstatAlerts, status, RED_H);
            } else if (status.getRawValue() < status.getRedLowLimit() && status.getComponent().equals(BATTERY_VAL)) {
                // Battery is too low.
                violation = checkIssue(batteryAlerts, status, RED_L);
            }
            // If we found a violation add it to the results
            if (violation != null) {
                results.add(violation);
            }
        }
        return results;
    }
    // Returns ResponseData if there is a violation, returns null if there is not.
    private static ResponseData checkIssue(Map<Integer, Deque<TelemetryData>> alerts, TelemetryData status, String condition) {
        // Find previous alerts for current interval.
        Deque<TelemetryData> cur = alerts.get(status.getSatelliteId());
        // The interval earliest value within the 5-minute interval.
        OffsetDateTime errorInterval = status.getTimestamp().plusMinutes(-INTERVAL);
        ResponseData report = null;

        if (cur == null) {
            // this means it is a new satellite's first value
            // add deque to map.
            cur = new ArrayDeque<>();
            alerts.put(status.getSatelliteId(), cur);
        }
        //remove satellite alerts that are older than five minutes
        while (!cur.isEmpty() && cur.peekLast().getTimestamp().isBefore(errorInterval)) {
            cur.pollLast();
        }
        cur.offerFirst(status);
        // if we are at or over the error limit (3+ errors) within error Interval,
        if (cur.size() >= ERROR_LIMIT) {
            // generate report based on the earliest time within the error interval.
            report = new ResponseData(cur.peekLast(), condition);
            // remove the last error, so that we don't get a violation of the same time as before.
            cur.pollLast();
        }
        return report;
    }
}

