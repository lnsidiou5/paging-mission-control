package com.enlightenchallenge.pagingmissioncontrol.entity;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class TelemetryData {
    OffsetDateTime timestamp;
    int satelliteId;
    double redHighLimit;
    double yellowHighLimit;
    double yellowLowLimit;
    double redLowLimit;
    double rawValue;
    String component;

    public TelemetryData(String[] input) {
        setTimestamp(input[0]);
        setSatelliteId(Integer.parseInt(input[1]));
        setRedHighLimit(Integer.parseInt(input[2]));
        setYellowHighLimit(Integer.parseInt(input[3]));
        setYellowLowLimit(Integer.parseInt(input[4]));
        setRedLowLimit(Integer.parseInt(input[5]));
        setRawValue(Float.parseFloat(input[6]));
        setComponent(input[7]);
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public double getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(double redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(double yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(double yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public double getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(double redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(float rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String stringDate) {
        try {
            // This is the format that the input is in.
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSSX");
            // add Z to denote UTC time zone. Otherwise, it uses the system's current timezone.
            Date date = inputFormat.parse(stringDate+"Z");
            // Correct the type and assign it to the timestamp
            timestamp = date.toInstant().atOffset(ZoneOffset.UTC);
        } catch (ParseException e) {
            // if there was a parsing error, don't store a timestamp.
            timestamp = null;
        }
    }
}
