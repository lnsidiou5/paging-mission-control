package com.enlightenchallenge.pagingmissioncontrol.entity;

import java.time.Instant;
import java.time.OffsetDateTime;

public class ResponseData {
    public ResponseData(TelemetryData status, String severity) {
        this.setComponent(status.getComponent());
        this.setSatelliteID(status.getSatelliteId());
        this.setSeverity(severity);
        this.setTimestamp(status.getTimestamp());
    }
    int satelliteID;
    String severity;
    String component;
    OffsetDateTime timestamp;

    public int getSatelliteID() {
        return satelliteID;
    }

    public void setSatelliteID(int satelliteID) {
        this.satelliteID = satelliteID;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String toJSON(String indent) {
        // Convert the object to a json with proper indentation.
        return indent + "{\n" +
                indent + "    \"satelliteId\": " + satelliteID +",\n" +
                indent + "    \"severity\": \"" + severity + "\",\n" +
                indent + "    \"component\": \"" + component +"\",\n" +
                indent + "    \"timestamp\": \"" + timestamp.toString() + "\"\n" +
                indent + "}";
    }
}
