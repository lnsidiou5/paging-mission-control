package com.enlightenchallenge.pagingmissioncontrol;

import com.enlightenchallenge.pagingmissioncontrol.entity.ResponseData;
import com.enlightenchallenge.pagingmissioncontrol.service.StatusService;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class PagingControl {
	private static final String INPUT_PATH = ".\\data\\input\\";
	public static void main(String[] args) throws IOException {
		// This code will take an input if available or default to the test case.
		String filename = "test1.txt";
		if (args !=null && args.length > 0 && args[0].length() != 0) {
			filename = args[0];
		}
		StatusService service = new StatusService();

		// read the input file
		File input = new File( INPUT_PATH + filename);
		List<ResponseData> result = service.getStatus(input);

		// Print the JSON string
		String resJSON = responsesToJSON(result);
		System.out.println(resJSON);
	}

	private static String responsesToJSON(List<ResponseData> result) {
		StringBuilder sb = new StringBuilder();
		// Add starting bracket
		sb.append("[\n");
		int i = 0;
		for (ResponseData data : result) {
			// Generate each data object
			sb.append(data.toJSON("    "));
			if (++i < result.size()) {
				// As long as it is not the last one, add a comma
				sb.append(",");
			}
			// Indent to a new line
			sb.append("\n");
		}
		// Add ending bracket and return
		sb.append("]");
		return sb.toString();
	}

}
